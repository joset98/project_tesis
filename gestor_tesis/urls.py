"""gestor_tesis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from tesis.views import *
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('home/', home.as_view(), name = 'home'),
    path('home/register/nota', csrf_exempt(createNota.as_view()), name = 'createn'),
    path('home/register/thesis', csrf_exempt(createThesis.as_view()), name = 'createt'),
    path('home/register/TA', csrf_exempt(createTA.as_view())),
    path('home/register/TI', csrf_exempt(createTI.as_view()), name = 'createti'),
    path('home/register/EST', csrf_exempt(createEST.as_view()), name = 'createest'),
    path('home/register/career',csrf_exempt(createCareer.as_view()), name = 'rgsc'),
    path('home/register/enterprise', csrf_exempt(rgs_enterprise.as_view()), name = 'rgse'),
    path('home/defending', csrf_exempt(createDef.as_view()), name = 'rgdef'),
    path('home/listauthor/', modify_delete_author.as_view(), name = 'mda'),
    path('home/listtutora/', modify_delete_tutora.as_view(), name = 'mdta'),
    path('home/listtutori/', modify_delete_tutori.as_view(), name = 'mdti'),
    path('home/listenterprise/', modify_delete_enterprise.as_view(), name = 'mde'),
    path('home/listdefending/', modify_delete_defending.as_view(), name = 'mde'),
    path('home/listcaarer/', modify_delete_career.as_view(), name = 'mdc'),
    path('home/listnotas/', modify_delete_nota.as_view(), name = 'mdn'),
    path('home/listthesis', modify_delete_thesis.as_view(), name = 'mdt'),
    path('home/detail/thesis', detailThesis, name = 'dt'),
    path('home/detail/periodthesis', listPeriodThesis, name ='lpt'),
    path('home/report', report, name='pt')

]