from django.forms import ModelForm
from django.forms import Form
from django import forms
from .models import *
from datetime import datetime
from django.contrib.admin import widgets

currentTime = datetime.now()

class formThesis(ModelForm):

	class Meta:
		model = Thesis
		fields = '__all__'
		exclude = ['notaDEF', 'inicio', 'final']

class formCareer(ModelForm):

	class Meta:
		model = Career
		fields = '__all__'


class formPerson(ModelForm):

	class Meta:
		model = Person
		fields = '__all__'
		exclude = ['types',]

class formTA(ModelForm):

	class Meta:
		model = TutorA
		fields = ['career',]

class formTI(ModelForm):

	class Meta:
		model = TutorI
		fields = ['enterprise',]

class formDefending(ModelForm):

	def __init__(self, *args, **kwargs):
		super(formDefending, self).__init__(*args, **kwargs)

		from django.db.models.expressions import RawSQL
		raw = None

		if len(Defending.objects.all()) == 0:
			raw = Thesis.objects.all()
		
		else:
			raw = RawSQL("SELECT NTESIS FROM TESIS_THESIS WHERE NOT NTESIS IN(SELECT THESIS_ID FROM TESIS_DEFENDING)",())

		self.fields['thesis'].queryset = Thesis.objects.filter(ntesis__in = raw)


	class Meta:
		model = Defending
		fields = '__all__'
		exclude = ['fecha', 'observaciones']

class formn(ModelForm):

	class Meta:
		model = jury_def
		fields = '__all__'

class formEst(ModelForm):

	class Meta:
		model = Author
		fields = ['career',]

class formEnterprise(ModelForm):

	class Meta:
		model = Enterprise
		fields = '__all__'

from django.forms import Form


class formDetailT(Form):

	career = forms.ModelChoiceField( Career.objects.all())
	
	


