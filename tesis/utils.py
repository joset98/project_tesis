from tesis.models import jury_def, Defending
import json
import datetime


def dictfetchall(cursor):
  
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def registerNot(objt, nota):
	
	notaF = float(objt.notaTA)*0.25 + float(objt.notaTI)*0.35 + float(nota)*0.40				
	objt.notaDEF = notaF
	if notaF >= 7.0:
		objt.status = 'apb'
	else:
		objt.status = 'rpb'

def verifyNotsJurys(objt):

	try:
		objd = Defending.objects.get(thesis = objt)
		jurynots = jury_def.objects.filter(defending = objd)
	except Exception:
		pass
	
	prom = 0
	count = 0

	for jurynot in jurynots:
		
		nota = jurynot.nota

		if nota:
			count += 1
			prom += float(nota)


	if count == 2:

		return prom/count

	return False
		

def registerThesis(objt, obja, objta, objti, obje):

	objt.author = obja
	objt.tutorA = objta
	objt.tutorI = objti
	objt.enterprise = obje
	objt.save()

def addModThesis(objt, obja, objta, objti, obje, datei, datef):

	if obja.career.name == objta.career.name and obje.riff == objti.enterprise.riff and datei < datef:
		
		objt.inicio = datei
		objt.final = datef

		if objt.status == 'elb':

			registerThesis(objt, obja, objta, objti, obje)

		elif objt.status == 'rvs':

			date = datetime.datetime.now()

			if date < datef and date > datei:

				registerThesis(objt, obja, objta, objti, obje)
			else:
				return False

		elif objt.status == 'ld' or objt.status == 'apb' or objt.status == 'rpb':
			
			if objt.notaTA and objt.notaTI:

				nota = verifyNotsJurys(objt)

				if nota:
					registerNot(objt, nota)	
				else:
					return False

				registerThesis(objt, obja, objta, objti, obje)

			else:
				registerThesis(objt, obja, objta, objti, obje)
				

	return True

def delete_object(object, data, index):

	data = json.loads(data)

	try:
		if index == 'cd':
			object.objects.get(cd = data[index]).delete()

		elif index == 'riff':
			object.objects.get(riff = data[index]).delete()

		elif index == 'ndef':
			object.objects.get(ndef = data[index]).delete()

		elif index == 'id':
			object.objects.get(id = data[index]).delete()

		return 'True'

	except Exception:

		return 'False'

def calculate_data(POST, condition):

	data1 = {}
	data2 = {}

	for obj in POST:

		if obj[0] != condition and obj[0] !='csrfmiddlewaretoken':
			data1[obj[0]] = obj[1]
		elif obj[0] == condition:
			data2[obj[0]] = obj[1]

	if data1 != {} or data2 != {}:
		return data1, data2

	return {}, {}

def  dateIsValid(inicio, final):

	fi = datetime.datetime.strptime(inicio.replace('-', '/'), '%Y/%m/%d')
	ff = datetime.datetime.strptime(final.replace('-', '/'), '%Y/%m/%d')

	if abs(fi.year - ff.year) != 0 or (abs(fi.year - ff.year) == 0 and (abs(fi.month - ff.month) < 4 or abs(fi.month - ff.month) > 6)):
		return False

	return True

def generate_pdf(data):

	from jinja2 import Environment, FileSystemLoader

	env = Environment(loader = FileSystemLoader("tesis"))
	template = env.get_template("static/html/tesis/thesis_list.html")

	html = template.render({'object_list': data})


	import pdfkit

	pdfkit.from_string(html,'reporte: %s.pdf'%data.ntesis)